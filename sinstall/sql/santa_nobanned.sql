CREATE TABLE IF NOT EXISTS `%PREFIX%_nobanned` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` bigint(15) DEFAULT NULL,
  `host` varchar(50) DEFAULT NULL,
  `block` int(1) DEFAULT '0',
  `log` int(1) DEFAULT '0',
  `notice` int(1) DEFAULT '0',
  `comment` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Список ботов, которх не баним';
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_nobanned` (`id`, `ip`, `host`, `block`, `log`, `notice`, `comment`) VALUES
	(1, NULL, '*.yandex.net', 1, 1, NULL, 'YandexBot'),
	(2, NULL, '*.googlebot.com', 1, 1, NULL, 'GoogleBot'),
	(3, NULL, '*.yahoo.com', 1, 1, NULL, 'YahooBot'),
	(4, NULL, '*.search.msn.com', 1, 1, NULL, 'BingBot'),
	(5, NULL, '*.yandex.ru', 1, 1, 0, 'YandexBot'),
	(6, NULL, '*.yandex.com', 1, 1, 0, 'YandexBot');

