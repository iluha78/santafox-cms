DROP TABLE IF EXISTS `%PREFIX%_comments1_reviews`;
-- sqlseparator------------------------------------------------
CREATE TABLE `%PREFIX%_comments1_reviews` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `pageid` text NOT NULL,
  `rate` tinyint(1) unsigned DEFAULT NULL COMMENT 'оценка 1..5',
  `pros` text COMMENT 'достоинства',
  `cons` text COMMENT 'недостатки',
  `comment` text COMMENT 'комментарий',
  `when` datetime NOT NULL COMMENT 'дата-время',
  `available` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'показываем на сайте?',
  PRIMARY KEY (`id`),
  KEY `pageid_when` (`pageid`(200),`when`,`available`),
  KEY `available_pageid_rate` (`pageid`(200),`rate`,`available`),
  KEY `when_available` (`when`,`available`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT='Ревью';
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_comments1_reviews` VALUES(9, 'Посетитель', 'catalog,itemid=5', 5, 'Описываем достоинства', 'Описываем недостатки', 'Текст комментария Текст комментария', '2014-11-01 15:07:17', 1);
