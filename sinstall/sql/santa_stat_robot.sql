DROP TABLE IF EXISTS `%PREFIX%_stat_robot`;
-- sqlseparator------------------------------------------------
CREATE TABLE `%PREFIX%_stat_robot` (
  `IDRobot` int(10) unsigned NOT NULL auto_increment,
  `robot` varchar(30) NOT NULL default '',
  `agent` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`IDRobot`)
) ENGINE=MyISAM AUTO_INCREMENT=331 DEFAULT CHARSET=utf8 COMMENT='Статистика - поисковые роботы';
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('1','Aport','aport');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('2','AWBot','awbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('3','BaiDuSpider','baiduspider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('4','Bobby','bobby');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('5','Boris','boris');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('6','Bumblebee (relevare.com)','bumblebee');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('7','CsCrawler','cscrawler');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('8','DaviesBot','daviesbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('9','ExactSeek Crawler','exactseek');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('10','Ezresult','ezresult');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('11','GigaBot','gigabot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('12','GNOD Spider','gnodspider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('13','Grub.org','grub');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('14','Mirago','henrythemiragorobot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('15','Holmes','holmes');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('16','InternetSeer','internetseer');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('17','JustView','justview');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('18','LinkBot','linkbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('19','MetaGer LinkChecker','metager\\-linkchecker');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('20','LinkChecker','linkchecker');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('21','Microsoft URL Control','microsoft_url_control');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('22','MSIECrawler','msiecrawler');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('23','Nagios','nagios');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('24','Perman surfer','perman');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('25','Pompos','pompos');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('26','StackRambler','rambler');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('27','Red Alert','redalert');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('28','Shoutcast Directory Service','shoutcast');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('29','SlySearch','slysearch');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('30','SurveyBot','surveybot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('31','Turn It In','turnitinbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('32','Turtle','turtlescanner');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('33','Turtle','turtle');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('34','Ultraseek','ultraseek');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('35','WebClipping.com','webclipping\\.com');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('36','webcompass','webcompass');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('37','Web Wombat Redback Spider','wonderer');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('38','Yahoo Vertical Crawler','yahoo\\-verticalcrawler');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('39','Yandex bot','yandex');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('40','ZealBot','zealbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('41','Zyborg','zyborg');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('42','Walhello appie','appie');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('43','ArchitextSpider','architext');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('44','AskJeeves','jeeves');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('45','Bjaaland','bjaaland');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('46','Wild Ferret Web Hopper #1, #2,','ferret');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('47','Googlebot','googlebot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('48','Northern Light Gulliver','gulliver');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('49','Harvest','harvest');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('50','ht://Dig','htdig');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('51','LinkWalker','linkwalker');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('52','Lycos','lycos_');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('53','moget','moget');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('54','Muscat Ferret','muscatferret');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('55','Internet Shinchakubin','myweb');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('56','Nomad','nomad');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('57','Scooter','scooter');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('58','Inktomi Slurp','slurp');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('59','Voyager','^voyager\\/');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('60','weblayers','weblayers');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('61','Antibot','antibot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('62','Digout4u','digout4u');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('63','EchO!','echo');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('64','Fast-Webcrawler','fast\\-webcrawler');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('65','Alexa (IA Archiver)','ia_archiver');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('66','JennyBot','jennybot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('67','Mercator','mercator');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('68','Netcraft','netcraft');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('69','Petersnews','petersnews');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('70','Unlost Web Crawler','unlost_web_crawler');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('71','Voila','voila');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('72','WebBase','webbase');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('73','WISENutbot','wisenutbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('74','Fish search','[^a]fish');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('75','ABCdatos BotLink','abcdatos');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('76','Acme.Spider','acme\\.spider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('77','Ahoy! The Homepage Finder','ahoythehomepagefinder');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('78','Alkaline','alkaline');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('79','Anthill','anthill');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('80','Arachnophilia','arachnophilia');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('81','Arale','arale');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('82','Araneo','araneo');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('83','Aretha','aretha');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('84','ARIADNE','ariadne');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('85','arks','arks');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('86','ASpider (Associative Spider)','aspider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('87','ATN Worldwide','atn\\.txt');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('88','Atomz.com Search Robot','atomz');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('89','AURESYS','auresys');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('90','BackRub','backrub');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('91','BBot','bbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('92','Big Brother','bigbrother');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('93','BlackWidow','blackwidow');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('94','Die Blinde Kuh','blindekuh');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('95','Bloodhound','bloodhound');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('96','Borg-Bot','borg\\-bot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('97','bright.net caching robot','brightnet');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('98','BSpider','bspider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('99','CACTVS Chemistry Spider','cactvschemistryspider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('100','Calif','calif[^r]');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('101','Cassandra','cassandra');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('102','Digimarc Marcspider/CGI','cgireader');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('103','Checkbot','checkbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('104','ChristCrawler.com','christcrawler');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('105','churl','churl');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('106','cIeNcIaFiCcIoN.nEt','cienciaficcion');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('107','Collective','collective');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('108','Combine System','combine');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('109','Conceptbot','conceptbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('110','CoolBot','coolbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('111','Web Core / Roots','core');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('112','XYLEME Robot','cosmos');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('113','Internet Cruiser Robot','cruiser');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('114','Cusco','cusco');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('115','CyberSpyder Link Test','cyberspyder');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('116','Desert Realm Spider','desertrealm');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('117','DeWeb(c) Katalog/Index','deweb');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('118','DienstSpider','dienstspider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('119','Digger','digger');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('120','Digital Integrity Robot','diibot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('121','Direct Hit Grabber','direct_hit');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('122','DNAbot','dnabot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('123','DownLoad Express','download_express');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('124','DragonBot','dragonbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('125','DWCP (Dridus\' Web Cataloging P','dwcp');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('126','e-collector','e\\-collector');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('127','EbiNess','ebiness');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('128','ELFINBOT','elfinbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('129','Emacs-w3 Search Engine','emacs');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('130','ananzi','emcspider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('131','Esther','esther');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('132','Evliya Celebi','evliyacelebi');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('133','FastCrawler','fastcrawler');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('134','Fluid Dynamics Search Engine r','fdse');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('135','Felix IDE','felix');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('136','FetchRover','fetchrover');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('137','fido','fido');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('138','Hдmдhдkki','finnish');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('139','KIT-Fireball','fireball');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('140','Fouineur','fouineur');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('141','Robot Francoroute','francoroute');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('142','Freecrawl','freecrawl');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('143','FunnelWeb','funnelweb');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('144','gammaSpider, FocusedCrawler','gama');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('145','gazz','gazz');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('146','GCreep','gcreep');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('147','GetBot','getbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('148','GetURL','geturl');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('149','Golem','golem');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('150','Grapnel/0.01 Experiment','grapnel');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('151','Griffon','griffon');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('152','Gromit','gromit');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('153','Gulper Bot','gulperbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('154','HamBot','hambot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('155','havIndex','havindex');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('156','Hometown Spider Pro','hometown');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('157','HTMLgobble','htmlgobble');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('158','Hyper-Decontextualizer','hyperdecontextualizer');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('159','iajaBot','iajabot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('160','Popular Iconoclast','iconoclast');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('161','Ingrid','ilse');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('162','Imagelock','imagelock');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('163','IncyWincy','incywincy');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('164','Informant','informant');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('165','InfoSeek Robot 1.0','infoseek');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('166','Infoseek Sidewinder','infoseeksidewinder');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('167','InfoSpiders','infospider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('168','Inspector Web','inspectorwww');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('169','IntelliAgent','intelliagent');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('170','I, Robot','irobot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('171','Iron33','iron33');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('172','Israeli-search','israelisearch');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('173','JavaBee','javabee');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('174','JBot Java Web Robot','jbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('175','JCrawler','jcrawler');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('176','JoBo Java Web Robot','jobo');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('177','Jobot','jobot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('178','JoeBot','joebot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('179','The Jubii Indexing Robot','jubii');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('180','JumpStation','jumpstation');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('181','image.kapsi.net','kapsi');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('182','Katipo','katipo');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('183','Kilroy','kilroy');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('184','KO_Yappo_Robot','ko_yappo_robot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('185','LabelGrabber','labelgrabber\\.txt');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('186','larbin','larbin');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('187','legs','legs');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('188','Link Validator','linkidator');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('189','LinkScan','linkscan');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('190','Lockon','lockon');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('191','logo.gif Crawler','logo_gif');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('192','Mac WWWWorm','macworm');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('193','Magpie','magpie');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('194','marvin/infoseek','marvin');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('195','Mattie','mattie');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('196','MediaFox','mediafox');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('197','MerzScope','merzscope');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('198','NEC-MeshExplorer','meshexplorer');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('199','MindCrawler','mindcrawler');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('200','mnoGoSearch search engine soft','mnogosearch');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('201','MOMspider','momspider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('202','Monster','monster');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('203','Motor','motor');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('204','MSNBot','msnbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('205','Muncher','muncher');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('206','Mwd.Search','mwdsearch');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('207','NDSpider','ndspider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('208','Nederland.zoek','nederland\\.zoek');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('209','NetCarta WebMap Engine','netcarta');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('210','NetMechanic','netmechanic');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('211','NetScoop','netscoop');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('212','newscan-online','newscan\\-online');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('213','NHSE Web Forager','nhse');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('214','The NorthStar Robot','northstar');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('215','nzexplorer','nzexplorer');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('216','ObjectsSearch','objectssearch');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('217','Occam','occam');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('218','HKU WWW Octopus','octopus');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('219','Openfind data gatherer','openfind');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('220','Orb Search','orb_search');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('221','Pack Rat','packrat');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('222','PageBoy','pageboy');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('223','ParaSite','parasite');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('224','Patric','patric');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('225','pegasus','pegasus');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('226','The Peregrinator','perignator');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('227','PerlCrawler 1.0','perlcrawler');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('228','Phantom','phantom');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('229','PhpDig','phpdig');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('230','PiltdownMan','piltdownman');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('231','Pimptrain.com\'s robot','pimptrain');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('232','Pioneer','pioneer');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('233','html_analyzer','pitkow');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('234','Portal Juice Spider','pjspider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('235','PlumtreeWebAccessor','plumtreewebaccessor');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('236','Poppi','poppi');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('237','PortalB Spider','portalb');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('238','psbot','psbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('239','The Python Robot','python');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('240','Raven Search','raven');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('241','RBSE Spider','rbse');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('242','Resume Robot','resumerobot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('243','RoadHouse Crawling System','rhcs');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('244','Road Runner: The ImageScape Ro','road_runner');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('245','Robbie the Robot','robbie');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('246','ComputingSite Robi/1.0','robi');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('247','RoboCrawl Spider','robocrawl');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('248','RoboFox','robofox');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('249','Robozilla','robozilla');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('250','Roverbot','roverbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('251','RuLeS','rules');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('252','SafetyNet Robot','safetynetrobot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('253','Sleek','search\\-info');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('254','Search.Aus-AU.COM','search_au');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('255','SearchProcess','searchprocess');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('256','Senrigan','senrigan');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('257','SG-Scout','sgscout');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('258','ShagSeeker','shaggy');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('259','Shai\'Hulud','shaihulud');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('260','Sift','sift');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('261','Simmany Robot Ver1.0','simbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('262','Site Valet','site\\-valet');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('263','SiteTech-Rover','sitetech');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('264','Skymob.com','skymob');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('265','SLCrawler','slcrawler');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('266','Smart Spider','smartspider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('267','Snooper','snooper');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('268','Solbot','solbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('269','Speedy Spider','speedy');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('270','spider_monkey','spider_monkey');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('271','SpiderBot','spiderbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('272','Spiderline Crawler','spiderline');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('273','SpiderMan','spiderman');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('274','SpiderView(tm)','spiderview');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('275','Spry Wizard Robot','spry');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('276','Site Searcher','ssearcher');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('277','Suke','suke');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('278','suntek search engine','suntek');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('279','Sven','sven');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('280','TACH Black Widow','tach_bw');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('281','Tarantula','tarantula');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('282','tarspider','tarspider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('283','TechBOT','techbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('284','Templeton','templeton');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('285','TITAN','titan');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('286','TitIn','titin');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('287','The TkWWW Robot','tkwww');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('288','TLSpider','tlspider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('289','UCSD Crawl','ucsd');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('290','UdmSearch','udmsearch');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('291','URL Check','urlck');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('292','Valkyrie','valkyrie');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('293','Verticrawl','verticrawl');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('294','Victoria','victoria');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('295','vision-search','visionsearch');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('296','void-bot','voidbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('297','VWbot','vwbot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('298','The NWI Robot','w3index');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('299','W3M2','w3m2');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('300','WallPaper (alias crawlpaper)','wallpaper');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('301','the World Wide Web Wanderer','wanderer');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('302','w@pSpider by wap4.com','wapspider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('303','WebBandit Web Spider','webbandit');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('304','WebCatcher','webcatcher');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('305','WebCopy','webcopy');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('306','webfetcher','webfetcher');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('307','The Webfoot Robot','webfoot');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('308','Webinator','webinator');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('309','WebLinker','weblinker');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('310','WebMirror','webmirror');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('311','The Web Moose','webmoose');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('312','WebQuest','webquest');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('313','Digimarc MarcSpider','webreader');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('314','WebReaper','webreaper');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('315','Websnarf','websnarf');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('316','WebSpider','webspider');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('317','WebVac','webvac');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('318','webwalk','webwalk');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('319','WebWalker','webwalker');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('320','WebWatch','webwatch');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('321','whatUseek Winona','whatuseek');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('322','WhoWhere Robot','whowhere');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('323','Wired Digital','wired\\-digital');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('324','w3mir','wmir');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('325','WebStolperer','wolp');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('326','The Web Wombat','wombat');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('327','The World Wide Web Worm','worm');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('328','WWWC Ver 0.2.5','wwwc');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('329','WebZinger','wz101');
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_stat_robot` VALUES ('330','XGET','xget');