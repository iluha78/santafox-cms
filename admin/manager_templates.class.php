<?php

/**
 * Менеджер редактирования и управления шаблонами
 *
 * @name manager_interface
 * @package manager_templates
 *
 * @author Vyshnyvetskyy Aleksandr (alex.vyshnyvetskyy@gmail.com)
 * @copyright W.D.M.Group (с) 2015 (alex.vyshnyvetskyy@wdmg.com.ua)
 * @release 16/05/2015
 * @version 1.0
 */

class manager_templates
{
	/**
	 * Конструктор класса
	 *
	 * @return manager_templates
	 */
	function __construct()
	{

	}

    /**
     * Предопределённая функция для формирования меню
     *
     * Функция орпеделяет какие элементы меню присутсвуют в меню раздела
     * @param pub_interface $show Объект класса pub_interface
     * @return void
     * @access private
     */
	function interface_get_menu($show)
    {
        $show->set_menu_block('[#templates_structure_label#]');
        $show->set_menu("[#templates_structure_template#]","structure_template");
        $show->set_menu("[#templates_structure_list#]","structure_list");
		
        $show->set_menu_block('[#templates_modules_label#]');
        $show->set_menu("[#templates_modules_template#]","module_template");
        $show->set_menu("[#templates_modules_list#]","modules_list");

        $show->set_menu_default('structure_list');
    }

    /**
     * Определяет какой из разделов АИ был запрошен и выполняет соответсвующую функцию
	 *
     * @return string $content
     * @access private
     */
	function start()
    {
    	global $kernel;
        $action = $kernel->pub_section_leftmenu_get();
		$html_content = '';
        switch ($action)
        {
            default:
            case 'structure_template': // Выводит форму для создания/редактирования шаблона страницы
            	$html_content = $this->templates_structure_template();
			break;
            case 'structure_list': // Выводит список шаблонов страниц доступных для редактирования
            	$html_content = $this->templates_structure_list();
			break;
            case 'module_template': // Выводит форму для создания/редактирования шаблона модуля
            	$html_content = $this->templates_module_template();
			break;
            case 'modules_list': // Выводит список шаблонов модулей доступных для редактирования
            	$html_content = $this->templates_modules_list();
			break;
            case 'template_save': // Сохраняет шаблон
            	$redirect = $kernel->pub_httpget_get('redirect');
				$template_patch = $kernel->pub_httpget_get('path');
            	$content = $kernel->pub_httppost_get('template_content');

				if (ADMIN_TPL_EDITOR == 'code')
				{
					if($this->template_save($template_patch, $content))
					{
						if($redirect)
						{
							$kernel->pub_redirect_refresh_reload($redirect . '&path=' . $template_patch);
						}
						else
						{
							$kernel->pub_redirect_refresh_reload('structure_template&path=' . $template_patch);
						}
					}
					else
					{
						if($redirect)
						{
							$kernel->pub_redirect_refresh_reload($redirect);
						}
						else
						{
							$kernel->pub_redirect_refresh_reload('structure_template');
						}
					}
				}
				else
				{
					if($this->template_save($template_patch, $content))
					{
						$data['success'] = '1';


						return json_encode($data);
					}
				}
			break;
            case 'template_rename': // Переименовывает шаблон
				$redirect = $kernel->pub_httpget_get('redirect');
				$template_patch = $kernel->pub_httpget_get('path');
				$new_filename = $kernel->pub_httpget_get('new_filename');
				$template_patch = $this->template_rename($template_patch, $new_filename);
				if($redirect)
					$kernel->pub_redirect_refresh_reload($redirect.'&path='.$template_patch);
				else
					$kernel->pub_redirect_refresh_reload('structure_template&path='.$template_patch);
			break;
            case 'template_clone': // Клонирует шаблон
				$redirect = $kernel->pub_httpget_get('redirect');
				$template_patch = $kernel->pub_httpget_get('path');
				$template_patch = $this->template_clone($template_patch);
				if($redirect)
					$kernel->pub_redirect_refresh_reload($redirect.'&path='.$template_patch);
				else
					$kernel->pub_redirect_refresh_reload('structure_template&path='.$template_patch);
			break;
            case 'template_delete': // Удаляет шаблон
				$redirect = $kernel->pub_httpget_get('redirect');
				$template_patch = $kernel->pub_httpget_get('path');
				$this->template_delete($template_patch);
				if($redirect)
					$kernel->pub_redirect_refresh($redirect);
				else
					$kernel->pub_redirect_refresh('structure_list');
			break;
        }
	    return $html_content;
    }
	
    /**
     * Выводит список шаблонов страниц доступных для редактирования
     *
     * @return string $content
     * @access private
    */
    private function templates_structure_list() {
		global $kernel;
		$list_template = $kernel->pub_template_parse("admin/templates/default/templates_structure_list.html");
    	$begin = $list_template['begin'];
    	$end = $list_template['end'];
		$content = '';
		$templates = $kernel->priv_templates_get($kernel->priv_path_page_template_get());
		foreach ($templates as $template) {
			$delete_link = $list_template['delete_link'];
			$clone_link = $list_template['clone_link'];
			$edit_link = $list_template['edit_link'];
			if($this->get_uses_in_pages($template)) {
				$is_uses = $list_template['uses'];
				$options = $clone_link.$edit_link.'<span style="display:inline-block;width:26px;height:26px;"></span>';
			} else {
				$is_uses = $list_template['not_uses'];
				$options = $clone_link.$edit_link.$delete_link;
			}
			$row = $list_template['row'];
			$row = str_replace('%template_path%', $template, $row);
			$row = str_replace('%is_uses%', $is_uses, $row);
			$options = str_replace('%template_path%', $template, $options);
			$row = str_replace('%options%', $options, $row);
			$content .= $row;
		};
		return $begin.$content.$end;
    }
	
    /**
     * Выводит список шаблонов модулей доступных для редактирования
     *
     * @return string $content
     * @access private
    */
    private function templates_modules_list() {
		global $kernel;
		$list_template = $kernel->pub_template_parse("admin/templates/default/templates_modules_list.html");
    	$begin = $list_template['begin'];
    	$end = $list_template['end'];
		$content = '';
		$modules = $kernel->pub_modules_get();
		foreach ($modules as $module => $val) {
			$templates = array();
			if($val["id_parent"]) {
				$templates = $kernel->priv_templates_get('modules/'.$val["id_parent"].'/templates_user');
				foreach ($templates as $template) {
					$delete_link = $list_template['delete_link'];
					$clone_link = $list_template['clone_link'];
					$edit_link = $list_template['edit_link'];
					if($this->get_uses_in_modules($module, $template)) {
						$is_uses = $list_template['uses'];
						$options = $clone_link.$edit_link.'<span style="display:inline-block;width:26px;height:26px;"></span>';
					} else {
						$is_uses = $list_template['not_uses'];
						$options = $clone_link.$edit_link.$delete_link;
					}
					$row = $list_template['row'];
					$row = str_replace('%module_base_name%', $val["caption"], $row);
					$row = str_replace('%template_path%', $template, $row);
					$row = str_replace('%is_uses%', $is_uses, $row);
					$options = str_replace('%template_path%', $template, $options);
					$row = str_replace('%options%', $options, $row);
					$row = str_replace('%module_id%', $module, $row);
					$content .= $row;
				};
			};
		};
		return $begin.$content.$end;
    }

    /**
     * Выводит форму для создания/редактирования шаблона страницы
     *
     * @return string $content
     * @access private
    */
    private function templates_structure_template() {
		global $kernel;
		$template = $kernel->pub_template_parse("admin/templates/default/templates_edit.html");
		$content = $template['body'];
		$content = str_replace('%section%', '[#templates_structure_label#]', $content);
		$blank_html = $template['page_blank'];
		$rename_template = $template['rename_template'];
		$rename_template_disable = $template['rename_template_disable'];
		$template_path = $kernel->pub_httpget_get('path'); 
		$editor = new edit_content();
		$editor->set_edit_name('template_content');

		if (ADMIN_TPL_EDITOR == 'html')
		{
			$editor->set_full_form();
		}
		else
		{
			$editor->set_only_code();
		}

		if($template_path) {
			$content = str_replace('%action%', $kernel->pub_redirect_for_form('template_save&path='.$template_path), $content);
			$content = str_replace('%title%', '[#templates_structure_edit#]', $content);
			$filename = basename($kernel->priv_file_full_patch($template_path));
			if(!$this->get_uses_in_pages($template_path)) {
				$content = str_replace('%rename_template%', $rename_template, $content);
				$content = str_replace('%fullpath%', './'.str_replace($filename, '', $template_path), $content);
				$content = str_replace('%template_path%', $template_path, $content);
				$content = str_replace('%filename%', $filename, $content);
				$content = str_replace('%redirect_section%', 'structure_template', $content);	
			} else {
				$content = str_replace('%rename_template%', $rename_template_disable, $content);
				$content = str_replace('%fullpath%', './'.$template_path, $content);	
			}
			$editor->set_content($this->template_read($kernel->priv_file_full_patch($template_path)));
		} else {
			$content = str_replace('%title%', '[#templates_structure_template#]', $content);
			$content = str_replace('%rename_template%', '', $content);
			$filename = 'tpl_new_'.rand(1,999).'.html';
			$new_template_path = PATH_PAGE_TEMPLATE.'/'.$filename;
			$content = str_replace('%fullpath%', './'.PATH_PAGE_TEMPLATE.'/', $content);
			$content = str_replace('%filename%', $filename, $content);
			$editor->set_content($blank_html);
			$content = str_replace('%action%', $kernel->pub_redirect_for_form('template_save&path='.$new_template_path), $content);
		}
		$content = $kernel->priv_page_textlabels_replace($content);
		$content = str_replace('%editor%', $editor->create(), $content);
		$kernel->priv_output($content, true);
		exit();
    }
	
    /**
     * Выводит форму для создания/редактирования шаблона модуля
     *
     * @return string $content
     * @access private
    */
    private function templates_module_template() {
		global $kernel;
		$template = $kernel->pub_template_parse("admin/templates/default/templates_edit.html");
		$content = $template['body'];
		$rename_template = $template['rename_template'];
		$rename_template_disable = $template['rename_template_disable'];
		$content = str_replace('%section%', '[#templates_modules_label#]', $content);
		$template_path = $kernel->pub_httpget_get('path'); 
		$module_id = $kernel->pub_httpget_get('module');
		
		if($template_path) {
			$module_caption = '';
			$modules = $kernel->pub_modules_get();
			foreach($modules as $module => $val)
				if($module==$module_id)
					$module_caption = ' "'.$val["caption"].'"';
				
			$content = str_replace('%action%', $kernel->pub_redirect_for_form('template_save&path='.$template_path.'&redirect=module_template'), $content);
			$content = str_replace('%title%', '[#templates_module_edit#]'.$module_caption, $content);
			$filename = basename($kernel->priv_file_full_patch($template_path));  
			
			if(!$this->get_uses_in_modules($module_id, $template_path)) {
				$content = str_replace('%rename_template%', $rename_template, $content);
				$content = str_replace('%fullpath%', './'.str_replace($filename, '', $template_path), $content);
				$content = str_replace('%template_path%', $template_path, $content);
				$content = str_replace('%filename%', $filename, $content);
				$content = str_replace('%redirect_section%', 'module_template', $content);	
			} else {
				$content = str_replace('%rename_template%', $rename_template_disable, $content);
				$content = str_replace('%fullpath%', './'.$template_path, $content);	
			}


			
			$editor = new edit_content();
			$editor->set_edit_name('template_content');

			if (ADMIN_TPL_EDITOR == 'html')
			{
				$editor->set_full_form();
			}
			else
			{
				$editor->set_only_code();
			}

			$editor->set_content($this->template_read($kernel->priv_file_full_patch($template_path)));
			$content = $kernel->priv_page_textlabels_replace($content);
			$content = str_replace('%editor%', $editor->create(), $content);
			$kernel->priv_output($content, true);
			exit();
		
		} else {
			if($module_id) {
				$filename = 'tpl_new_'.rand(1,999).'.html';
				$new_template_path = './modules/'.$module_id.'/templates_user/'.$filename;
				$blank_html = $template['module_blank'];
				$editor = new edit_content();
				$editor->set_edit_name('template_content');

				if (ADMIN_TPL_EDITOR == 'html')
				{
					$editor->set_full_form();
				}
				else
				{
					$editor->set_only_code();
				}

				$editor->set_content($blank_html);
				$module_caption = '';
				$modules = $kernel->pub_modules_get();
				foreach($modules as $module => $val)
					if($val["id_parent"]==$module_id)
						$module_caption = ' "'.$val["caption"].'"';
					
				$content = str_replace('%action%', $kernel->pub_redirect_for_form('template_save&path='.$new_template_path.'&redirect=module_template'), $content);
				$content = str_replace('%title%', '[#templates_modules_template#]'.$module_caption, $content);
				$content = str_replace('%rename_template%', '', $content);
				$content = str_replace('%fullpath%', './modules/'.$module_id.'/templates_user/', $content);
				$content = str_replace('%filename%', $filename, $content);
				$content = str_replace('%editor%', $editor->create(), $content);
			
			} else { // значит модуль не указан, предлагаем выбрать
				$modules_list = '';
				$modules = $kernel->pub_modules_get();
				$module_select = $template['module_select'];
				
				foreach($modules as $module => $val)
					if($val["id_parent"] && $val["id_parent"] != 'evalmod' && $val["id_parent"] != 'linkfeed' && $val["id_parent"] != 'pageprint' && $val["id_parent"] != 'sentinel')
						$modules_list .= '<option value="'.$val["id_parent"].'">'.$val["caption"].'</option>';
					
				$content = str_replace('%title%', '[#templates_modules_template#]', $content);
				$module_select = str_replace('%selects%', $modules_list, $module_select);
				$content = str_replace('%rename_template%', $module_select, $content);
				$content = str_replace('%editor%', '', $content);
				$content = str_replace('%action%', $kernel->pub_redirect_for_form('template_save&redirect=module_template'), $content);
			}
		}
		
		$content = $kernel->priv_page_textlabels_replace($content);
		$kernel->priv_output($content, true);
		exit();
    }

    /**
     * Проверяет, используется ли шаблон в какой либо странице
     *
     * @param (string) $template Путь к файлу шаблона
     * @return bolean
     * @access private
    */
    private function get_uses_in_pages($template) {
		global $kernel;
		$pages = $kernel->pub_mapsite_get();
		foreach ($pages as $page)
			if(array_key_exists("template", $page["properties"]))
				if ($page["properties"]["template"]==$template)
				return true;			
		return false;
    }

    /**
     * Проверяет, используется ли шаблон в каком либо действии модуля
     *
     * @param (string) $id_module Идентификатор модуля шаблон которого проверяется
     * @param (string) $template Путь к файлу шаблона
     * @return bolean
     * @access private
    */
    private function get_uses_in_modules($id_module, $template) {
		global $kernel;
		$props = array();
        $actions = $kernel->db_get_list_simple('_action', 'id_module = "'.$id_module.'"');
        foreach($actions as $row)
			$props[] = unserialize($row['param_array']);
		foreach($props as $prop)
			if (array_search($template,$prop))
				return true;
		return false;
    }

    /**
     * Переименовывает файл шаблон и, если необходимо, увеличивает счетчик-префикс имени файла
     *
     * @param (string) $old_filename Путь к файлу шаблона
     * @param (string) $new_filename Новое имя файла шаблона
     * @return (string) имя переименованого файла шаблона или старое имя в случае неудачи
     * @access private
    */
    private function template_rename($old_filename, $new_filename) {
		global $kernel;
		$new_template_patch = '';
		if ($old_filename && $new_filename) {
			$new_template_patch = dirname($old_filename);
			$new_filename = $new_template_patch.'/'.$kernel->pub_translit_string($new_filename);
			if (file_exists($kernel->priv_file_full_patch($new_filename))) {
				$new_filename = preg_replace('/_(\d+).html/i', '', $new_filename);
				$new_filename = str_replace('.html', '', $new_filename);
				for ($i=1; $i++<99;) {
					$new_custom_patch = $new_filename.'_'.$i.'.html';
					if (!file_exists($kernel->priv_file_full_patch($new_custom_patch))) {
						$new_filename = $new_custom_patch;
						break;
					};
				};
			};
			if($kernel->pub_file_rename($old_filename, $new_filename))
				return $new_filename;
			else
				return $old_filename;
		};
		return $old_filename;
	}
	
    /**
     * Клонирует шаблон и увеличивает счетчик-префикс имени файла
     *
     * @param (string) $template_patch Путь к файлу шаблона
     * @return (string) имя клонированого файла шаблона или текущее имя в случае неудачи
     * @access private
    */
    private function template_clone($template_patch) {
		global $kernel;
		$new_template_patch = $template_patch;
		if ($template_patch) {
			$new_template_filename = preg_replace('/_(\d+).html/i', '', $template_patch);
			$new_template_filename = str_replace('.html', '', $new_template_filename);
			for ($i=1; $i++<99;) {
				$new_template_patch = $new_template_filename.'_'.$i.'.html';
				if (!file_exists($kernel->priv_file_full_patch($new_template_patch)))
				break;
			}
		}
		if($kernel->pub_file_copy($template_patch, $new_template_patch))
			return $new_template_patch;
		else
			return $template_patch;
	}
	
    /**
     * Удаляет шаблон из папки назначения
     *
     * @param (string) $template_patch Путь к файлу шаблона
     * @access private
    */
    private function template_delete($template_patch) {
		global $kernel;
		if ($template_patch)
			$kernel->pub_file_delete($template_patch, false);
	}
	
    /**
     * Читает шаблон в папке назначения
     *
     * @param (string) $template_patch Путь к файлу шаблона
     * @return (string/bolean) содержимое файла шаблона или false в случае неудачи
     * @access private
    */
    private function template_read($template_patch) {
        global $kernel;
		$template_content = '';
        if (!empty($template_patch) && file_exists($template_patch))
   			return file_get_contents($template_patch);
		else
			return false;
    }
	
    /**
     * Сохраняет шаблон в папку назначения
     *
     * @param (string) $template_patch Путь к файлу шаблона
     * @param (string) $template_content Содержимое сохраняемого шаблона
     * @return (bolean) true в случае удачного сохранения или false в случае неудачи
     * @access private
    */
    private function template_save($template_patch, $template_content) {
        global $kernel;
        $template_full_patch = $kernel->priv_file_full_patch($template_patch);
        if ($kernel->pub_file_save($template_full_patch, stripcslashes($template_content)))
			return true;
		else
			return false;
    }
}