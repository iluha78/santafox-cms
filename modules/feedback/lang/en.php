<?php

$type_langauge = 'en';

$il['feedback_modul_base_name'] = 'Contact Us';

$il['feedback_menu_label'] = 'Manage form';
$il['feedback_menu_edit_ini'] = 'Generation template';
$il['feedback_create_template'] = 'Create a form template';
$il['feedback_create_template_winlabel'] = 'File name template';
$il['feedback_create_template_label'] = 'Enter the name of the new template file using only letters and the sign "_"';


$il['feedback_pub_show_form'] = 'Show the form';
$il['feedback_property_label_template'] = 'The template form';
$il['feedback_property_label_email'] = 'Email Manager';
$il['feedback_property_label_type'] = 'Type the letter';
$il['feedback_property_label_html'] = 'HTML';
$il['feedback_property_label_text'] = 'Plain text';

$il['feedback_property_field_yes'] = 'Yes';
$il['feedback_property_field_no'] = 'No';
$il['feedback_property_label_name'] = 'Name the store manager';
$il['feedback_property_label_theme'] = 'The theme sends you a message';
$il['feedback_field_label'] = 'The name of the field in the form';
$il['feedback_field_regexp'] = 'Regular expression';
$il['feedback_field_blank'] = 'Obl.';
$il['feedback_field_type'] = 'Field type';
$il['feedback_field_type_blank1'] = 'rows (input)';
$il['feedback_field_type_blank2'] = 'A tick (checkbox)';
$il['feedback_field_type_blank3'] = 'The area (textarea)';
$il['feedback_field_regexp_blank1'] = 'number';
$il['feedback_field_regexp_blank2'] = 'E-mail';
$il['feedback_field_regexp_blank3'] = 'string';
$il['feedback_field_regexp_noselect'] = 'Not selected';

$il['feedback_property_messages_perpage'] = 'Message page in AI';
$il['feedback_messages_label'] = 'View';
$il['feedback_menu_message_list'] = 'All messages';
$il['feedback_messages_list'] = 'List of messages';
$il['feedback_message_pubdate'] = 'Submit';
$il['feedback_message_name'] = 'Manager name';
$il['feedback_message_email'] = 'Email manager';
$il['feedback_message_theme'] = 'Subject';
$il['feedback_message_message'] = 'Report';
$il['feedback_message_status'] = 'Send status';
$il['feedback_message_action_check_all'] = 'Select all';
$il['feedback_message_action_uncheck_all'] = 'Remove selection';
$il['feedback_message_action_invertcheck_all'] = 'Invert selection';
$il['feedback_message_action_delete'] = 'Delete';
$il['feedback_message_page_first'] = 'Home';
$il['feedback_message_page_last'] = 'End';
$il['feedback_messages_list_null'] = 'There are no posts to display';
$il['feedback_messagelist_status_success'] = 'Success';
$il['feedback_messagelist_status_error'] = 'Error';

$il['feedback_field_id'] = 'ID field';
$il['feedback_field_saving'] = 'Save';
$il['feedback_field_waiting'] = 'The data is stored, wait.';
$il['feedback_field_info'] = 'Information';
$il['feedback_field_failure'] = 'Error saving';
$il['feedback_field_new'] = 'New Field';
$il['feedback_field_add'] = 'Add';
$il['feedback_item_action_remove'] = 'Delete';
$il['feedback_item_remove'] = 'The field will only be removed from the form. Existing templates will not be affected';