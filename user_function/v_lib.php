<?php
/**
 * Пользовательские функции
 * Пользовательские функции предназначены для вызова функций в шаблоне с передачей параметров и возвратом результата.
 * Для вызова пользовательской функции в шаблоне необходимо прописать такую конструкцию [@function-<название функции>(<передаваемая ей строка>)]
 */

/**
 * Функция v_request($a) предназначена для вывода в шаблона значения из $_REQUEST.
 * Пример вызова [@function-v_request(page)]
 * Если вы параметр $_REQUEST[$name] отсутсвует вы можете вывести значение по умолчанию [@function-v_request(page ||| default_value)]
 * @param string $name
 * @param string $default
 * @return null|string
 */
function v_request($a){
    list($name,$default) = explode(" ||| ",$a);
    if(isset($_REQUEST[$name]))
        return $_REQUEST[$name];
    else
        return "".$default;
}

/**
 * Функция v_selected_print($a) предназначена для сохранения состояний выпадающих списков.
 * Пример вызова [@function-v_selected_print(name_select ;;||;; value)]
 * Данную функцию необходимо писать в каждом из <option> - <option value="55" [@function-v_selected_print(name_select ;;||;; 55)]>
 * Выводит ' selected="selected"' если $_REQUEST[$name]==$value
 * @param $name
 * @param $value
 * @return string
 */
function v_selected_print($a){
    list($name,$value) = explode(" ;;||;; ",$a);
    if(isset($_REQUEST[$name]) && $_REQUEST[$name]==$value) return ' selected="selected"'; else return '';
}

/**
 * Функция v_checked_print($a) предназначена для сохранения состояния чекбоксов и радиокнопок.
 * Пример вызова [@function-v_checked_print(name_сhekbox)].
 * Например, <input type="checkbox" name="my_checkbox" [@function-v_checked_print(my_checkbox)]>
 * Выводит checked="checked" если  существует $_REQUEST[$name]
 * @param $name
 * @return string
 */
function v_checked_print($name){
    if(isset($_REQUEST[$name])) return ' checked="checked"'; else return '';
}
